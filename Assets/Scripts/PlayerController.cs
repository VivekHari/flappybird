﻿using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float Gravity = 0.005f;
	public float JumpVelocity = 1.0f;

	private float velocity;
	private bool wasJumping = false;

	// Use this for initialization
	void Start () {
		velocity = 0;
	}
	
	// Update is called once per frame
	void Update () {
		velocity -= Gravity;
		transform.Translate(new Vector3(0, velocity, 0));

		bool isJumping = Input.GetButton("Jump");
		if (isJumping && !wasJumping) {
			velocity = JumpVelocity;
		}
		wasJumping = isJumping;
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Pipe") {
			Destroy(gameObject);
		}
    }
}

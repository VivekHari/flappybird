﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreKeeper : MonoBehaviour {

    public float scoreMultiplier;
    public Text scoreText;

    private int score;

	void Start () {

	}
	
    void Update()
    {
        score = Mathf.RoundToInt(Time.time * scoreMultiplier);
        scoreText.text = score.ToString() + "m";
    }
}

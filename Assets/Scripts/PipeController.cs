﻿using UnityEngine;

public class PipeController : MonoBehaviour {

	public float Speed = 1.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(-Speed, 0, 0);

		if (transform.position.x < -10) {
			transform.position = new Vector3(
				transform.position.x + 20, transform.position.y, transform.position.z);
		}
	}
}
